% get training and testing data
% i -- represent i th quarter, 
% j -- represent j th bus 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function train_test_dat = fetch_data(P_mat, i, j, blocksize)
train_test_dat = P_mat((blocksize*(i-1) + 1): (blocksize*i) , j);
end 
