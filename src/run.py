__author__ = 'markxchen'

import util
import alg
import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt
import pickle

def execute():
    # dat = util.load_data('../dat/power_8buses.dat')
    dat = util.load_data('../dat/longitudinal_favor_loads.csv')
    # print dat.index
    BLOCK_SIZE = 24*7*13
    # print dat.iloc[:, 2]
    dates = pd.date_range('1/1/2014', periods=(4*BLOCK_SIZE+23), freq='H')
    # print len(dates)
    dat.index = dates

    # i = 0  # season - quarter 0, 1, 2, 3
    # j = 2 # bus index : 1-7
    #
    error_dict = {}
    y_hat_dict = {}
    for i in range(4):
        for j in range(1,8):
            data_learn = dat.iloc[ i*BLOCK_SIZE:(i+1)*BLOCK_SIZE , j ]
            # avg_err, pred_vals = alg.ts_alg( data_learn )
            # error_dict[(i,j)] = avg_err
            # y_hat_dict[(i,j)] = pred_vals
            avg_err, pred_vals = alg.linear_regression(data_learn, j, i)
            error_dict[(i,j)] = avg_err
            y_hat_dict[(i,j)] = pred_vals

            # break
        # plt.show()
        # break

    print error_dict
    # output_err = open('res/ts_pred_err.pkl', 'w')
    output_err = open('res/lin_pred_err.pkl', 'w')
    pickle.dump(error_dict, output_err)
    # output_y_hat = open('res/ts_y_hat.pkl', 'w')
    output_y_hat = open('res/lin_y_hat.pkl', 'w')
    pickle.dump(y_hat_dict, output_y_hat)


def post_read():
    # err = pickle.load(open('res/ts_pred_err.pkl', 'r'));
    err = pickle.load(open('res/lin_pred_err.pkl', 'r'));
    err2 = pickle.load(open('res/lin_pred_err2.pkl', 'r'));
    # y_hat = pickle.load(open('res/ts_y_hat.pkl', 'r'));
    y_hat = pickle.load(open('res/lin_y_hat.pkl', 'r'));
    y_hat2 = pickle.load(open('res/lin_y_hat2.pkl', 'r'));

    print err
    # print err2
    # print y_hat

# execute()
# post_read()

def execute_plot(m, n):
    # dat = util.load_data('../dat/power_8buses.dat')
    # dat = util.load_data('../dat/longitudinal_favor_loads.csv')
    # print dat.index
    BLOCK_SIZE = 24*7*13
    # print dat.iloc[:, 2]

    print dat 
    # raise NotImplemented("manual break ! ");

    dates = pd.date_range('1/1/2014', periods=(4*BLOCK_SIZE+23), freq='H')
    # print len(dates)
    dat.index = dates

    # i = 0  # season - quarter 0, 1, 2, 3
    # j = 2 # bus index : 1-7
    #
    error_dict = {}
    y_hat_dict = {}
    for i in range(n-1,n):
        for j in range(m-1,m):
            data_learn = dat.iloc[ i*BLOCK_SIZE:(i+1)*BLOCK_SIZE , j ]
            avg_err, pred_vals = alg.linear_regression(data_learn, i, j)

    plt.show()

#####################
# run for plot
####################
# execute_plot(8,1)
# execute_plot(6,2)
# execute_plot(4,4)

#####################

def re_exec(n, filepath = None):
    # dat = util.load_data('../dat/longitudinal_favor_loads.csv')
    # dat = util.load_data('../dat/cross_sectional_favor_loads_7buses_06021333.csv')
    # dat = util.load_data('../dat/cross_sectional_favor_loads_7buses_06021359.csv')
    #cross_sectional_favor_loads_7buses_06021359.csv
    if filepath is not None : 
        dat = util.load_data(filepath)
        filename = filepath.split('/')[-1]
    else :
        dat = util.load_data('../dat/cross_sectional_favor_loads_7buses_06021359.csv')
        filename = 'cross_sectional_favor_loads_7buses_06021359.csv'

    BLOCK_SIZE = 24*7*13

    dates = pd.date_range('1/1/2014', periods=(4*BLOCK_SIZE+24), freq='H')
    dat.index = dates 


    
    # print dat 
    # raise NotImplemented("break !!")
    j = 8 ;

    # errs = [] 
    for i in range(n-1,n):
        data_learn = dat.iloc[ i*BLOCK_SIZE:(i+1)*BLOCK_SIZE , j]
        avg_err, pred_vals = alg.avg_regress(data_learn, i, j, filename)
        # errs.append(avg_err)


        # avg_err, pred_vals = alg.linear_regression(data_learn, i, j)
        # print data_learn

    # plt.show()    
    # return errs 
    return avg_err

# re_exec(1)
# re_exec(2)
# re_exec(3)
# re_exec(4)

def run_in_patch():
    err_mat = None 
    for i in range(176):
        errs = [] 
        for j in range(1, 5):
            filename = '../dat/loads_7buses/%d.csv' % i 
            err = re_exec(j, filename)
            errs.append(err)   
        if err_mat is None :     
            err_mat = errs     
        else : 
            err_mat = np.vstack((err_mat, errs))    
        # break 
        # plt.show()
        # if i > 10 : break 
    # plt.show()    
    # break     
    print err_mat

    plt.figure()
    plt.plot(err_mat[:,1], 'ko',err_mat[:,3], 'ko' )
    plt.ylabel('RMSE')
    plt.xlabel('File Name')
    plt.show()

run_in_patch()

# re_exec(2)
# plt.show()