__author__ = 'markxchen'

from scipy import stats
import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import pandas as pd
from statsmodels.graphics.api import qqplot
import sys, os

def ts_alg(dat):
    print "inside ts algorithm function!"
    # print dat
    WEEK_BLOCK = 24*7
    dat_training = dat.iloc[:-WEEK_BLOCK]
    dat_testing = dat.iloc[-WEEK_BLOCK:]
    # print dat_training.index[-1]
    pq = pick_order(dat_training)
    ###############################
    ##### residual qq-plot ######
    # resid = ts_model.resid;
    # fig = plt.figure(figsize=(12,8))
    # ax = fig.add_subplot(111)
    # fig = qqplot(resid, line='q', ax=ax, fit=True)
    # plt.show()
    ###############################

    prediction_vector = [];
    for i in range(len(dat_testing)-1):
        # ts_model = sm.tsa.ARMA(dat.iloc[i:-WEEK_BLOCK+i], pq).fit(verbose=False)
        if i == 0 :
            training_d = dat.iloc[i:-WEEK_BLOCK]
            print "**********************\n"

        elif i > 0 :
            # training_d = training_d.append(prediction_vector)
            # training_d = dat.iloc[i:-WEEK_BLOCK+i]
            training_d = training_d.append(pd.Series(prediction_vector[i-1], index=[dat_testing.index[i-1]]))
            # training_d = training_d.append(pd.Series(prediction_vector[i-1], ignore_index=True))
        ts_model = sm.tsa.ARMA(training_d, pq).fit(verbose=True)
        pred = ts_model.predict(str(dat_testing.index[i]), str(dat_testing.index[i+1]), dynamic=True)
        prediction_vector.append(pred[0])
    prediction_vector.append(pred[1])

    #####################
    diff = np.abs(prediction_vector - dat_testing)
    errs = np.divide(diff, dat_testing)

    print "============================== "
    print "==== mean errors : %.4f ===== " % np.mean(errs)

    fig, ax = plt.subplots(figsize=(12, 8))
    # ax.plot(dat.index[WEEK_BLOCK*12-3:], dat.iloc[WEEK_BLOCK*12-3:]) #  = .plot(ax=ax)
    # # print len(dat_testing.index), len(prediction_vector)
    # # print dat_testing.index, prediction_vector
    ax.plot(dat_testing.index, np.array(prediction_vector), label='prediction')
    ax.plot(dat_testing.index, dat_testing.values, 'm', label='real')
    # ax.plot(errs)
    plt.legend(loc='best')
    # plt.show()

    return [np.mean(errs), prediction_vector]


def pick_order(dat_training):

    AIC_pq = {}
    BIC_pq = {}
    for p in range(1,6):
        for q in range(1):
            ts_model = sm.tsa.ARMA(dat_training, (p, q)).fit()
            AIC_pq[(p, q)] = ts_model.aic;
            BIC_pq[(p, q)] = ts_model.bic;

    ###############################
    ##### display acf / pacf ######
    # fig = plt.figure(figsize=(12,8))
    # ax1 = fig.add_subplot(211)
    # fig = sm.graphics.tsa.plot_acf(dat_training.values.squeeze(), lags=40, ax=ax1)
    # ax2 = fig.add_subplot(212)
    # fig = sm.graphics.tsa.plot_pacf(dat_training, lags=40, ax=ax2)
    #
    # plt.show()
    ################################

    from operator import itemgetter
    # print sorted(AIC_pq.items(), key=itemgetter(1))
    max_aic = sorted(AIC_pq.items(), key=itemgetter(1))[-1]
    # print sorted(BIC_pq.items(), key=itemgetter(1))
    max_bic = sorted(BIC_pq.items(), key=itemgetter(1))[-1]

    order_pq, aic_val = max_aic

    return order_pq


def linear_regression(dat, m, n):
    print "****inside LS algorithm function!****"
    WEEK_BLOCK = 24*7
    DAY_BLOCK = 24
    dat_training = dat.iloc[:-WEEK_BLOCK]
    dat_testing = dat.iloc[-WEEK_BLOCK:]

    # print len(dat_training.index.hour)
    # print dat_training.index.hour
    # print dat_training.index.weekday
    # print len(dat_training.index.weekday)
    # print dat_training.index.month
    # print len(dat_training.index.month)
    # print np.ones(WEEK_BLOCK).dot(np.mean(dat_training.iloc[:24]))
    last_week_avg = np.ones(WEEK_BLOCK).dot(np.mean(dat_training.iloc[:24]))
    # print len(last_week_avg)
    print "==============="
    for j in range(11):
        week_avg = np.ones(WEEK_BLOCK).dot(np.mean(dat_training.iloc[(WEEK_BLOCK*j):WEEK_BLOCK*(j+1)]))
        # print len(week_avg)
        # print week_avg
        last_week_avg = np.hstack((last_week_avg, week_avg))

    # print len(last_week_avg)
    # print dat_training.index.minute
    ##### last day avg #######################
    # last_day_avg = np.ones(DAY_BLOCK).dot(np.mean(dat_training.iloc[11:12]))
    days_length = 12*7  # 12*7 = 84
    # for d in range(days_length-1):
    #     day_avg = np.ones(DAY_BLOCK).dot(np.mean(dat_training.iloc[(DAY_BLOCK*d):(DAY_BLOCK*(d+1)) ]));
    #     last_day_avg = np.hstack((last_day_avg, day_avg))

    # print len(last_day_avg)
    ##########################################
    # last half day avg
    last_half_day_avg = np.ones(DAY_BLOCK/2).dot(np.mean(dat_training.iloc[5:6]))
    for h in range(days_length*2-1):
        half_day_avg = np.ones(DAY_BLOCK/2).dot(np.mean(dat_training.iloc[(DAY_BLOCK/2)*h : (DAY_BLOCK/2)*(h+1)]))
        last_half_day_avg = np.hstack((last_half_day_avg, half_day_avg))

    ###############################
    # a quarter day feature
    last_six_hr_avg = np.ones(DAY_BLOCK/4).dot(np.mean(dat_training.iloc[3:4]))
    for s in range(days_length*4-1):
        six_hr_avg = np.ones(DAY_BLOCK/4).dot(np.mean(dat_training.iloc[(DAY_BLOCK/4)*s :(DAY_BLOCK/4)*(s+1) ] ))
        last_six_hr_avg = np.hstack((last_six_hr_avg, six_hr_avg))

    ##############################
    # hour of the week

    hour_of_week = dat_training.index.weekday * 24 + dat_training.index.hour
    # print hour_of_week.shape
    # print hour_of_week

    mat = dat_training.index.hour
    mat = np.vstack((mat, dat_training.index.weekday))
    mat = np.vstack((mat, dat_training.index.month  /10))
    mat = np.vstack((mat, hour_of_week/10 ))
    mat = np.vstack((mat, last_week_avg))
    # mat = np.vstack((mat, last_day_avg))
    # mat = np.vstack((mat, last_half_day_avg))
    mat = np.vstack((mat, last_six_hr_avg))
    # print mat

    # print mat.shape

    mat = np.transpose(mat)

    from sklearn.preprocessing import PolynomialFeatures
    poly = PolynomialFeatures(degree=2)
    X = poly.fit_transform(mat)
    X = sm.add_constant(X, prepend=False)
    # print mat

    # print mat.shape

    y = dat_training.reshape((len(dat_training), 1))

    # print y.shape

    # model = sm.OLS(y, mat).fit()
    model = sm.OLS(y, X).fit()

    # print model.summary()
    print model.params

    ### plot training figures ###
    # plt.figure()
    # plt.plot(dat_training)
    # plt.plot(model.predict())

    ######################
    ### DEBUG ############
    ######################
    # plt.show()
    # raise NotImplementedError('Manual Break !!')
    ############################
    # testing setup
    mat_t = dat_testing.index.hour
    mat_t =  np.vstack((mat_t, dat_testing.index.weekday))
    mat_t =  np.vstack((mat_t, dat_testing.index.month /10 ))
    # hour of the week
    hr_of_week_t = dat_testing.index.weekday * 24 + dat_testing.index.hour
    mat_t = np.vstack(( mat_t, hr_of_week_t/10 ))
    ####################

    week_avg_t = np.ones(WEEK_BLOCK).dot(np.mean(dat_training.iloc[(WEEK_BLOCK*11):(WEEK_BLOCK*12)]))
    # print len(week_avg_t)
    mat_t =  np.vstack((mat_t, week_avg_t))
    ##### last day feature
    # last_day_avg_t = np.ones(DAY_BLOCK).dot(np.mean(dat_training.iloc[(12*WEEK_BLOCK-DAY_BLOCK):]))
    # for d in range(1,7):
    #     day_avg_t = np.ones(DAY_BLOCK).dot(np.mean(dat_training.iloc[(11*WEEK_BLOCK-DAY_BLOCK + d*DAY_BLOCK):(11*WEEK_BLOCK-DAY_BLOCK + (d+1)*DAY_BLOCK )]))
    #     last_day_avg_t = np.hstack((last_day_avg_t, day_avg_t))
    #
    # mat_t = np.vstack((mat_t, last_day_avg_t))
    ### end of last day feature ##################
    ##############
    #### half-day feature
    # last_half_day_avg_t = np.ones(DAY_BLOCK/2).dot(np.mean(dat_training.iloc[(12*WEEK_BLOCK-DAY_BLOCK):(12*WEEK_BLOCK-DAY_BLOCK/2) ]));
    half_day_size = 7*2 ## last week has 7 days, thus it contains 7*2 = 14 half-days
    # for h in range(1,half_day_size):
    #     half_day_avg_t = np.ones(DAY_BLOCK/2).dot(np.mean( dat_training.iloc[(11*WEEK_BLOCK + h*DAY_BLOCK/2): (11*WEEK_BLOCK + (h+1)*DAY_BLOCK/2) ] ))
    #     last_half_day_avg_t = np.hstack((last_half_day_avg_t, half_day_avg_t))
    
    # mat_t = np.vstack((mat_t, last_half_day_avg_t))
    #################################
    # last six hour avg
    last_six_hr_avg_t = np.ones(DAY_BLOCK/4).dot(np.mean(dat_training.iloc[(12*WEEK_BLOCK-DAY_BLOCK):(12*WEEK_BLOCK-DAY_BLOCK +DAY_BLOCK/4) ] ))

    for s in range(1, half_day_size*2):
        six_hr_avg_t = np.ones(DAY_BLOCK/4).dot(np.mean( dat_training.iloc[(11*WEEK_BLOCK-DAY_BLOCK + s*DAY_BLOCK/4): (11*WEEK_BLOCK-DAY_BLOCK + (s+1)*DAY_BLOCK/4)] ))
        last_six_hr_avg_t = np.hstack((last_six_hr_avg_t, six_hr_avg_t))

    mat_t = np.vstack((mat_t, last_six_hr_avg_t ))
    #############

    # print mat_t.shape
    # print len(model.params)

    mat_t = np.transpose(mat_t);
    X_test = poly.fit_transform(mat_t)
    X_test = sm.add_constant(X_test, prepend=False)

    X_test = np.transpose(X_test)
    # pred = model.params.dot(mat_t)
    pred = model.params.dot(X_test)
    # print pred


    plt.figure()
    plt.plot(dat_testing, 'k', label='Act.')
    plt.plot(pred, 'r', label='Pred.')
    plt.title('Bus = %d, Qtr = %d ' %(n, m+1) );
    plt.ylabel('Power (p.u.)')
    plt.xlabel('Time step')
    # plt.show()
    ############
    # 0.00136819145276   poly order = 3
    # 0.00198102759156   poly order = 2
    ###################
    # 0.0056309097381  # poly order = 3
    # print np.mean((dat_testing-pred)**2)
    print np.sqrt(np.mean((dat_testing-pred)**2))
    mse = np.mean((dat_testing-pred)**2)
    print np.sqrt(mse)
    rmse = np.sqrt(mse)
    y_hat = pred

    return [rmse, y_hat]


def avg_regress(dat, m, n, filename=None):
    # print "****inside LS algorithm function!****"
    WEEK_BLOCK = 24*7
    DAY_BLOCK = 24
    dat_training = dat.iloc[:-WEEK_BLOCK]
    dat_testing = dat.iloc[-WEEK_BLOCK:]

    print "==========================\n"
    # print len(dat_training)/DAY_BLOCK 

    days_in_training = len(dat_training)/DAY_BLOCK 
    
    # print days_in_training

    daily_load_sum_vector  = []
    day_of_week_vector = [] 
    month_vector = []
    cubic_mat = np.ones(len(dat_training)).reshape(7, 12, 24);
    for i in range(days_in_training): 
        daily_load = np.sum(dat_training.iloc[i*DAY_BLOCK:(i+1)*DAY_BLOCK] )
        daily_load_sum_vector.append(daily_load)
        day_of_week_vector.append(np.mean( dat_training.index.weekday[i*DAY_BLOCK:(i+1)*DAY_BLOCK ]) )
        month_vector.append(np.mean( dat_training.index.month[i*DAY_BLOCK:(i+1)*DAY_BLOCK ] ))
        daily_shape = dat_training.iloc[i*DAY_BLOCK:(i+1)*DAY_BLOCK] / daily_load
        j = i % 7 # day of week 
        # raise NotImplementedError("-------break !!!---------")
        k = i / 7 
        # print (j, k)   
        # print len(daily_shape)
        cubic_mat[j, k, :] = daily_shape.values
        # print daily_shape 
        # print "+++++++++++++++++++\n"
        # print cubic_mat[2,3,:]
        # print cubic_mat[j,k,:] 

    # raise NotImplementedError('Manual')    
    X = np.vstack((day_of_week_vector, month_vector))
    X = np.transpose(X)    
    # X = np.array(day_of_week_vector).reshape(days_in_training, 1)

    y = np.array(daily_load_sum_vector).reshape(days_in_training,1)

    # print X 
    # print y
    model = sm.OLS(y, X).fit()

    # print model.summary()
    # print model.params
    # plt.figure()
    # plt.plot(X, y, 'o')
    # plt.plot(dat_training)
    # plt.show()
    ########################

    days_in_test = len(dat_testing) / DAY_BLOCK 
    # print days_in_test 

    day_load_sum_t = []
    for i in range(days_in_test):
        # print dat_testing.index # .weekday # [i*DAY_BLOCK:(i+1)*DAY_BLOCK]

        # tmp = np.array([np.mean( dat_testing.index.weekday[i*DAY_BLOCK:(i+1)*DAY_BLOCK] ), np.mean( dat_testing.index.month[i*DAY_BLOCK:(i+1)*DAY_BLOCK]) ] )       
        # print tmp 
        day_load_sum = model.params.dot( np.array([np.mean( dat_testing.index.weekday[i*DAY_BLOCK:(i+1)*DAY_BLOCK] ), np.mean( dat_testing.index.month[i*DAY_BLOCK:(i+1)*DAY_BLOCK]) ] ))       
        day_load_sum_t.append(day_load_sum)

        # cubic_mat.mean(axis=1)
    # print day_load_sum_t     
    avg_normed_daily_shape = cubic_mat.mean(axis=1)

    # print avg_normed_daily_shape.shape  
    predictions = [] 
    for i in range(days_in_test):
        # print avg_normed_daily_shape[i, :] * day_load_sum_t[i] 
        pred = avg_normed_daily_shape[i, :] * day_load_sum_t[i] 
        predictions = np.hstack((predictions, pred))

    # print predictions    
    # plt.figure()
    # plt.plot(dat_testing, 'k', label='Act.')
    # plt.plot(predictions, 'r', label='Pred. ')
    # plt.legend(loc = 'best')
    # plt.show()

    mse = np.mean((dat_testing-predictions)**2)
    print np.sqrt(mse)
    rmse = np.sqrt(mse)
    if rmse < 0.11 :
        plt.figure()
        plt.plot(dat_testing, 'k', label='Act.')
        plt.plot(predictions, 'r', label='Pred. ')
        plt.title('Bus = %d, Qtr = %d ' %(n, m+1) );
        if filename is not None :
            plt.xlabel(str(filename))
        plt.legend(loc = 'best')

    y_hat = predictions

    return [rmse, y_hat]
    




