%%%%%%%%%%%%%%%%%%%%%%
%%%% more content %%%%
%%%%%%%%%%%%%%%%%%%%%%
clc;clear;
addpath('~/Documents/Stanford_Course_Taken/2015spring/CEE272R/272r_course_project/cee272r_project/src/');
% load '../dat/data.mat';
load '~/Documents/Stanford_Course_Taken/2015spring/CEE272R/272r_course_project/cee272r_project/dat/data.mat';

% disp(fieldnames(data{1}))
dims = size(data);

P_mat = zeros(dims(1), 8);

for i = 1:8    
    for j = 1:dims(1) 
        P_mat(j, i) = data{j}.bus(i, 3);
%         data{j}.bus
%         break
    end    
%     disp(p)
%     figure()
%     plot(p)
%     break
end     
       
size(P_mat)


% block_size = 13*7*24; % number of rows for each quarter 
% weekly_size = 7*24 ; % one week data size 

% dat_1_2 = fetch_data(P_mat, 1, 2, block_size);

% disp(dat_1_2)
% bus2_train_1 = P_mat(1:block_size, 2);
% 
% bus2_train_1 = P_mat(block_size+1:block_size, 2);

csvwrite('power_8buses.dat',P_mat);